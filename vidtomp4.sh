#!/bin/bash

function print_usage ()
{
	echo "Syntax: `basename $0` [source] [encoded]"
	echo "Ouput: mp4"
}

case $1 in
	'-h' | '--help')
		print_usage
		exit 0
		;;
	*)
		if [ -z $1 ] || [ -z $2 ]; then
			print_usage 1>&2
			exit 1
		fi
		src_video=$1
		enc_video=$2
		;;
esac

ffmpeg -y -i ${src_video} -pass 1 -threads auto -s 640x480 -vcodec libx264 -b 384k -bt 192k -f mp4 -flags +loop -cmp +chroma -partitions 0 -me_method epzs -subq 1 -trellis 0 -refs 1 -coder 0 -me_range 16 -g 300 -keyint_min 30 -sc_threshold 40 -i_qfactor 0.71 -maxrate 10M -bufsize 10M -rc_eq 'blurCplx^(1-qComp)' -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -level 30 -aspect 4:3 -an /dev/null
ffmpeg -y -i ${src_video} -pass 2 -threads auto -s 640x480 -vcodec libx264 -b 384k -bt 192k -f mp4 -flags +loop -cmp +chroma -partitions +parti4x4+partp4x4+partp8x8+partb8x8 -me_method umh -subq 6 -trellis 2 -refs 1 -coder 0 -me_range 16 -g 300 -keyint_min 30 -sc_threshold 40 -i_qfactor 0.71 -maxrate 10M -bufsize 10M -rc_eq 'blurCplx^(1-qComp)' -qcomp 0.6 -qmin 10 -qmax 51 -qdiff 4 -level 30 -aspect 4:3 -acodec libfaac -ab 96k -ac 1 ${enc_video}

exit 0
